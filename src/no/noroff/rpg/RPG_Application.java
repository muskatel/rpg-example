package no.noroff.rpg;
import no.noroff.rpg.characters.*;
import no.noroff.rpg.items.weapons.Sword;

public class RPG_Application
{
    public static void main(String[] args) {

        Ranger r = new Ranger();
        r.LevelUp();
        System.out.println(r);

        Mage m = new Mage();
        m.Equip(new Sword());

        System.out.println(m);
    }
}
