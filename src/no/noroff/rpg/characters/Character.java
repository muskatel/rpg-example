package no.noroff.rpg.characters;

import no.noroff.rpg.characters.attributes.Statistic;
import no.noroff.rpg.items.Item;
import no.noroff.rpg.items.weapons.Sword;

import java.util.ArrayList;

public abstract class Character {

    protected ArrayList<Statistic> stats = new ArrayList<>();
    protected ArrayList<Item> items = new ArrayList<Item>();
    private ArrayList<Statistic> calc_stats = new ArrayList<>();

    public abstract void LevelUp();

    @Override
    public String toString()
    {
        Calc_Stats();
        String out = "";

        for(Statistic stat : stats)
        {

            out += calc_stats.toString() + "\n";
        }

        return out;
    }

    void Calc_Stats()
    {
        //clear the list
        calc_stats = new ArrayList<>();

        for (Statistic stat: stats)
        {
            // add a copy to calc stats
            calc_stats.add(stat);

            // update based on items
            for (Item item : items)
            {
                if(stat.Name == item.BonusAttribute)
                {
                    stat.Value += item.Bonus;
                }
            }
        }
    }

    public void Equip(Item item)
    {
        if((this instanceof Mage) && (item instanceof Sword))
        {
            System.out.println("Forbidden combo!");
        }
        else {
            items.add(item);
        }
    }
}
