package no.noroff.rpg.characters.attributes;

public class Dexterity extends Statistic
{
    public Dexterity(int value)
    {
        this.Name = Attribute.Dexterity;
        this.Value = value;
    }
}
