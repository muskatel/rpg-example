package no.noroff.rpg.characters.attributes;

public class Strength extends Statistic
{
    public Strength(int value)
    {
        this.Name = Attribute.Stength;
        this.Value = value;
    }
}
