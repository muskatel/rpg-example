package no.noroff.rpg.characters.attributes;

import no.noroff.rpg.characters.attributes.Attribute;

public abstract class Statistic
{
    public Attribute Name;
    public int Value;

    @Override
    public String toString() {
        return "Statistic{" +
                "Name=" + Name +
                ", Value=" + Value +
                '}';
    }
}
