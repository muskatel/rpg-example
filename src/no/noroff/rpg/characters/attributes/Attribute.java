package no.noroff.rpg.characters.attributes;

public enum Attribute {
    Stength,        // = 0
    Dexterity,      // = 1
    Intelligence    // = 2
}
