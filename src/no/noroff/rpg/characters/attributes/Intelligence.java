package no.noroff.rpg.characters.attributes;

public class Intelligence extends Statistic
{
    public Intelligence(int value)
    {
        this.Name = Attribute.Intelligence;
        this.Value = value;
    }
}
