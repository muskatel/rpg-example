package no.noroff.rpg.characters;

import no.noroff.rpg.characters.attributes.*;

public class Ranger extends Character{

    public Ranger()
    {
        stats.add(new Strength(1));
        stats.add(new Dexterity(7));
        stats.add(new Intelligence(1));
    }

    public void LevelUp()
    {
        for (Statistic stat: stats) {

            if(stat.Name == Attribute.Stength)
            {
                stat.Value +=1;
            }

            if(stat.Name == Attribute.Dexterity)
            {
                stat.Value +=5;
            }

            if(stat.Name == Attribute.Intelligence)
            {
                stat.Value +=1;
            }
        }
    }
}
