package no.noroff.rpg.items;

import no.noroff.rpg.characters.attributes.Attribute;

public abstract class Item
{
    public String Name;

    public int Bonus;               // +1
    public Attribute BonusAttribute;   // Strength
}
