package no.noroff.rpg.items.weapons;

import no.noroff.rpg.characters.attributes.Attribute;
import no.noroff.rpg.items.Item;

public class Sword extends Item
{
    public Sword()
    {
        this.Name = "Cool Sword";
        this.Bonus = 4;
        this.BonusAttribute = Attribute.Stength;
    }
}
